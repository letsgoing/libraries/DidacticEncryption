/**************************************************************************
    @file     didacticNet.h
    @author   anian buehler @ letsgoING
**************************************************************************/


#ifndef _DIDACTICENC_
#define _DIDACTICENC_

#include "Arduino.h"


class DidacticEncSymetric
{
	#define DE_LENGHT_KEY			8

	#define MAX_LEN_CYPHERTEXT	DE_LENGHT_KEY*4
	#define MAX_LEN_PLAINTEXT	DE_LENGHT_KEY*4

	#define DE_ERROR_NOERROR          1
	#define DE_ERROR_KEYLENGHT_LONG  -1
	#define DE_ERROR_KEYLENGHT_SHORT -2

	private:
	char _bufferCyphertext[MAX_LEN_CYPHERTEXT +1] = {0};
	char _bufferPlaintext[MAX_LEN_PLAINTEXT +1] = {0};
	char _key[DE_LENGHT_KEY +1] = {'0','0','0','0','0','0','0','0','\0'};

	public:
	DidacticEncSymetric();
	~DidacticEncSymetric();

	int  setKey(char*);
	void crypt(char*, int,  char*, char*, int);
	void crypt(char*, char*, char*);
	void crypt(char*, char*);
};


class  DidacticEncAsymetric
{
	#define MAX_NR_OF_PRIMES    48
	#define MAX_PRIME_OFFSET    3 	
	#define MAX_LEN_PLAINTEXT   20
	#define MAX_LEN_CYPHERTEXT  250
	
	#define DE_CYPHER_SEPARATOR   ";"

	private:
	const int prime[MAX_NR_OF_PRIMES] = { 17, 19, 23, 29, 31, 37,
                                    41, 43, 47, 53, 59, 61, 67, 71, 73,
                                    79, 83, 89, 97, 101, 103, 107, 109,
                                    113, 127, 131, 137, 139, 149, 151, 157,
                                    163, 167, 173, 179, 181, 191, 193, 197,
                                    199, 211, 223, 227, 229, 233, 239, 241,
                                    251};//, 257, 263, 269, 271, 277, 281};

	char _bufferCyphertext[MAX_LEN_CYPHERTEXT +1] = {0};

	long _privateKey = 0L;
	long _publicKey  = 0L;
	long _rsaModule  = 0L;

	int _randomPin = A5;

	void generateRsaKeys(long, long, long&, long&, long&);
	long cryptRsa(long key, long rsaModul, long message);
	
	long getPrimeWithOffset(long);
	long getPrime();
	

	//rsa math functions
	long gcd (long, long);                //basic euclidean algorithm
	long egcd (long, long, long&, long&); //extended euclidean algorithm
	long modPow(long, long, long);        //modular exponentation

	public:
	DidacticEncAsymetric();
	~DidacticEncAsymetric();

	void generateKeys();

	void setRandomSeedPin(int);

	long getPrivateKey();
	long getPublicKey();
	long getRsaModule();

	int encrypt(char*, int, char*, long, long);
	int encrypt(char*, char*, long, long);

	int decrypt(char*, char*, long, long);
};
#endif
