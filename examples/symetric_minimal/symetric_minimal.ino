#include <DidacticEnc.h>

DidacticEncSymetric symEncrypt;

void setup() {
Serial.begin(9600);

char cypherText[MAX_LEN_CYPHERTEXT] ={0};
char plainText[MAX_LEN_PLAINTEXT] ={0};

char key[] = "12345678";
Serial.print("Key:\t\t");Serial.println(key);

char text[] = "Hallo letsgoING";
Serial.print("Original:\t");Serial.println(text);

//Verchluesseln
symEncrypt.crypt(text, cypherText, key);
Serial.print("Verschluesselt:\t");Serial.println(cypherText);

//Entschluesseln
symEncrypt.crypt(cypherText, plainText, key);
Serial.print("Entschlüsselt:\t");Serial.println(plainText);
}

void loop() {
  // put your main code here, to run repeatedly:
}
